import en from './i18n/en/index.json';
import es from './i18n/es/index.json';
import pt from './i18n/pt/index.json';

let translations = {
  en: en,
  es: es,
  pt: pt
};

export const getTranslations = () => {
 return translations;
};