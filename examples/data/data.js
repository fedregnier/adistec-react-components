

export const getListsData = () => {
    let data = [
        {
            'id': 1,
            'firstName': 'Juan',
            'lastName': 'Gomez',
            'email': 'juangomez@example.com',
            'creationDate': '2018-10-01T03:00:00.000Z',
            'sex': 'Male',
            'favoriteColors': ['Red', 'Blue']
        },
        {
            'id': 2,
            'firstName': 'Juan',
            'lastName': 'Gonzalez',
            'email': 'pgo@example.com',
            'creationDate': '2018-09-01T03:00:00.000Z',
            'sex': 'Male',
            'favoriteColors': ['Blue','Green']
        },
        {
            'id': 3,
            'firstName': 'Ramon',
            'lastName': 'Rodriguez',
            'email': 'rrodriquez@example.com',
            'creationDate': '2018-10-01T03:00:00.000Z',
            'sex': 'Male',
            'favoriteColors': ['Yellow', 'Black', 'Brown']
        },
        {
            'id': 4,
            'firstName': 'Juan',
            'lastName': 'Perez',
            'email': 'jperez@example.com',
            'creationDate': '2018-10-01T03:00:00.000Z',
            'sex': 'Female',
            'favoriteColors': ['White', 'Blue', 'Yellow']
        },
        {
            'id': 5,
            'firstName': 'Margarita',
            'lastName': 'Rodriguez',
            'email': 'mr@example.com',
            'creationDate': '2018-04-01T03:00:00.000Z',
            'sex': 'Female',
            'favoriteColors': ['Green']
        },
        {
            'id': 6,
            'firstName': 'Ramiro',
            'lastName': 'Sosa',
            'email': 'rloro@example.com',
            'creationDate': '2016-03-01T03:00:00.000Z',
            'sex': 'Male',
            'favoriteColors': ['Red', 'Blue']
        },
        {
            'id': 7,
            'firstName': 'Miguel',
            'lastName': 'Gonzalez',
            'email': 'mgonzalez@example.com',
            'creationDate': '2016-10-01T03:00:00.000Z',
            'sex': 'Male',
            'favoriteColors': ['Red', 'Blue', 'Green']
        },
        {
            'id': 8,
            'firstName': 'Teresa',
            'lastName': 'Gonzalez',
            'email': 'tgonzalez@example.com',
            'creationDate': '2019-10-05T03:00:00.000Z',
            'sex': 'Female',
            favoriteColors: ['Yellow', 'Blue', 'Green', 'Black']
        },
        {
            'id': 9,
            'firstName': 'Leandro',
            'lastName': 'Sosa',
            'email': 'lsosa@example.com',
            'creationDate': '2018-10-04T03:00:00.000Z',
            'sex': 'Male',
            'favoriteColors': ['Yellow']
        },
        {
            'id': 10,
            'firstName': 'Rosa',
            'lastName': 'Gonzalez',
            'email': 'rgonzalez@example.com',
            'creationDate': '2018-10-01T03:00:00.000Z',
            'sex': 'Female',
            'favoriteColors': ['Green']
        },
        {
            'id': 11,
            'firstName': 'Elena',
            'lastName': 'Sosa',
            'email': 'elena@example.com',
            'creationDate': '2019-10-03T03:00:00.000Z',
            'sex': 'Female',
            'favoriteColors': ['Red']
        }
    ];

    return data;
};

