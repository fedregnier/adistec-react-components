module.exports = {
  plugins: [
	require('cssnano')({ 
		preset: ['default', 
		{
			discardComments: { removeAll: true },
			discardDuplicates:{ removeAll:true }   	
		}
		]
	}),
	require('postcss-simple-vars')(),
	require('postcss-nested')(),
	require('postcss-cssnext')()
  ],
  extract: true,
  minimize: true
};
