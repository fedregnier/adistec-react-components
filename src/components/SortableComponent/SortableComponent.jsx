import React from 'react';
import { SortableContainer, SortableElement, arrayMove } from 'react-sortable-hoc';


const propTypes = {
    buildElement: React.PropTypes.function,
    items: React.PropTypes.array.isRequired,
    onUpdateItems: React.PropTypes.function
};

const SortableItem = SortableElement(({ element,index,items,i }) =>
    (<div>
       {element}
    </div>)
);

class List extends React.Component {
    constructor(props) {
        super(props);
    }

    render(){
        return(
            <div>
                {this.props.items.length > 0 ?
                    <div>
                        {this.props.items.map((item,index) =>
                            <SortableItem key={`item-${index}`} index={index} i={index} element={this.props.buildElement(item, index)} items={this.props.items}/>
                        )}
                    </div>
                    : null}
            </div>);
    }
}

const SortableList = SortableContainer(List, { withRef: true });

class SortableComponent extends React.Component {
    constructor(props) {
        super(props);
    }

    onSortEnd = ({ oldIndex, newIndex }) => {
        if(oldIndex != newIndex){
            this.props.onUpdateItems(arrayMove(this.props.items, oldIndex, newIndex));
        };
    };

    render(){
        return (
            <SortableList items={this.props.items} buildElement={this.props.buildElement} onSortEnd={this.onSortEnd}/>
        );
    }
}

SortableComponent.propTypes = propTypes;

export default SortableComponent;