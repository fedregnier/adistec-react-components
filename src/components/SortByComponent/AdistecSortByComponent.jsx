import React from 'react';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import orderBy from 'lodash/orderBy';
import { FormattedMessage } from 'react-intl';
import './AdistecSortByComponent.scss';
import styled from 'styled-components';


const StyledSelect = styled(SelectField)`
    width: 100% !important;
    box-sizing: border-box !important;    
    height: 61px !important;
    
    /* Select floating label */
    label {
        top: 45% !important;
        font-size:12px;
    }
    
    /* Select content */
    div > div {
        bottom: 10px !important;
        margin-top: 0 !important;
    }
    
    /* Select arrow */
    button {
        bottom: 10px !important;
    }
    
    /* Select error field */
    div:nth-child(4) {
        bottom: 18px !important;
    }
`;


const propTypes = {
    data: React.PropTypes.array.isRequired,
    items: React.PropTypes.array.isRequired,
    onChangeSort: React.PropTypes.func.isRequired   
};


class SortByComponent extends React.Component {
    constructor(props) {
        super(props); 
    	this.handleChange = this.handleChange.bind(this);
    	this.state = { value: null,originalOrder:this.props.data };
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.data.length !== prevProps.data.length) {
            this.setState({ originalOrder:this.props.data });
        }
    }

    handleChange = (event, index, criteria) => {
   		this.setState({ value:criteria });
   		this.props.onChangeSort(orderBy(this.props.data,[criteria.split(';')[0]],[criteria.split(';')[1] ? criteria.split(';')[1] : 'asc']));
   	}

    handleReset = () => {
        this.setState({ value:null });
        this.props.onChangeSort(this.state.originalOrder);
    }

    render() {
    	return(
    		<div>
				<div className={this.props.containerClass}>
					<h5 className="title">
						<span className="fa fa-sort-amount-asc" />
						<strong><FormattedMessage id="sortByComponent.sortBy" /></strong>
						<div className="title-buttons">
							{/*<button type="button" className="text-button" onClick={this.handleReset}>RESET</button>*/}
						</div>
					</h5>
					<div className="filter-group">
						<div className="row mb0">
							<div className="col s12 adistec-sort-by-component">
								<span type="button" className="clear-button" onClick={this.handleReset}><i className="fa fa-times" aria-hidden="true" /></span>
								<StyledSelect floatingLabelText={this.props.floatingLabelText} value={this.state.value} onChange={this.handleChange.bind(this)} className={this.props.iconMode ? 'icon-mode' : null}>
                                    {this.props.items ? this.props.items.map((option, k)=>{
                                        return <MenuItem key={k} className={option.iconMode ? 'icon-mode' : null} value={option.criteria + ';' + (option.order ? option.order : null)} primaryText={option.label}/>;})
                                        : null}
								</StyledSelect>
							</div>
						</div>
	                </div>
				</div>
			</div>
    	);
    }
}

SortByComponent.propTypes = propTypes;

export default SortByComponent;
