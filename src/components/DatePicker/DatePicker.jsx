import React from 'react';
import DatePicker  from 'redux-form-material-ui/es/DatePicker';
import { Field, change , untouch , touch } from 'redux-form';
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import ClearButtonTextField from '../TextField/ClearButtonTextField.jsx';

const propTypes = {
    clearButton: React.PropTypes.bool,
    defaultDate: React.PropTypes.object,
    disabled: React.PropTypes.bool,
    intl:React.PropTypes.object,
    label: React.PropTypes.string,
    name: React.PropTypes.string,
    size:React.PropTypes.array.string,
    validate:React.PropTypes.array
};


const textFieldStyle =  { width: '100% ', boxSizing: 'border-box ',height:'61px ',marginBottom: '14px ' };
const inputStyle = { marginTop: '12px',height:'100%' };
const floatingLabelStyle =  { top: '45%', fontSize: '14px', whiteSpace: 'nowrap' };

class AdistecDatepicker extends React.Component {
    constructor(props) {
        super(props);
    }

    getFormName = () => {
        return this.refs[this.props.name].getRenderedComponent().props.meta.form;   
    }

    reset = () => {  
        this.props.resetDatePicker(this.getFormName(),this.props.name,null);
        this.props.untouchDatePicker(this.getFormName(),this.props.name);
    }

    setTouched = () => {
        this.props.touchDatePicker(this.getFormName(),this.props.name);
    }

    render() {
        return (
             <div className={this.props.size ? 'col s' + this.props.size : ''} style={{ position:'relative' }}>
                <Field name={this.props.name} floatingLabelText={this.props.label}  
                    withRef
                    ref={this.props.name}
                    format={(value, name) => value === '' ? null : value}
                    component={DatePicker}
                    defaultDate={this.props.defaultDate? this.props.defaultDate : undefined}
                    formatDate={this.props.intl.formatDate}
                    onChange={(event, newValue, previousValue) => {
                        !this.refs[this.props.name].getRenderedComponent().props.meta.touced ? this.setTouched() : null;
                    }}
                    disabled={this.props.disabled}
                    size={this.props.size}
                    textFieldStyle={textFieldStyle}
                    inputStyle={inputStyle}
                    floatingLabelStyle={floatingLabelStyle}
                    validate={this.props.validate}
                />
                {this.props.clearButton ? <ClearButtonTextField onClick={this.reset}/> : null}
            </div>
        );
    }
}

AdistecDatepicker.propTypes = propTypes;

AdistecDatepicker = connect(null, {
  resetDatePicker: (formName,fieldName,value) => change( formName, fieldName, value ),
  untouchDatePicker: (formName,fieldName) => untouch(formName,fieldName),
  touchDatePicker: (formName,fieldName) => touch(formName,fieldName)
})(AdistecDatepicker);


export default injectIntl(AdistecDatepicker);