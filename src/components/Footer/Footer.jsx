import React from 'react';
import './Footer.scss';

const Footer = () => (
    <footer>
        <p>Copyright © {(new Date()).getFullYear()} Adistec Corp. All rights reserved.</p>
    </footer>
);

export default Footer;