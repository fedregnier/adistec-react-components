import React from 'react';
import TablePager from './TablePager.jsx';
import Workbook from 'react-excel-workbook';
import './AdistecTable.scss';
import orderBy from 'lodash/orderBy';
import forEach from 'lodash/forEach';
import isEmpty from 'lodash/isEmpty';

const propTypes = {
    afterOrder:React.PropTypes.function,
    columns:React.PropTypes.array.isRequired,
    containerClass: React.PropTypes.string,
    excelConfiguration: React.PropTypes.object,
    initialPage: React.PropTypes.number,
    pageSize:React.PropTypes.number,
    pagerContianerClass:React.PropTypes.string,
    renderCurrentPage:React.PropTypes.bool,
    rows: React.PropTypes.array.isRequired,
    rowsClass:React.PropTypes.array, //array of functions
    tableContainerClass:React.PropTypes.string,
    tdClickFunction: React.PropTypes.function,
    trDoubleClickFunction: React.PropTypes.function
};


const defaultProps = {
    includePager: true
};


class AdistecTable extends React.Component {
    constructor(props) {
        super(props);
        let orderBy = {};
        this.props.columns.map((c,index) => {
            if(c.sortable){
                orderBy[index] = {};
                orderBy[index]['order'] = c.defaultSort ? c.defaultSort : 'desc'; 
                orderBy[index]['active'] = c.defaultSort ? true : false;
            }
        });
        this.state = {
            pageOfItems: [],
            rows:[],
            orderBy:orderBy
        };

        this.onChangePage = this.onChangePage.bind(this);
    }

    componentWillMount() {
        this.setState({ rows: this.orderDefaultRows() });
    }

    orderDefaultRows = () => {
        let rows = this.props.rows;
        let key = -1;
        forEach(this.state.orderBy, function(orderBy,k){
            if(orderBy.active){
                key = k;
            }
        });

        if(key != -1){
            let column = this.props.columns[key];
            let criteria = typeof(column.sortable) == 'function' ? eval(column.sortable) : column.accessor;
            rows = orderBy(rows, [criteria], [this.state.orderBy[key]['order']]);
            
            if(this.props.afterOrder){
                this.props.afterOrder(orderRows);
            }
        }

        return rows;
    }

    componentDidUpdate(prevProps) {
        if (defaultProps.includePager && prevProps.rows != this.props.rows) {
            this.setState({ rows: this.orderDefaultRows() });
        }
    }

    onChangePage = (pageOfItems) => {
        this.setState({ pageOfItems: pageOfItems });
    }

    sortBy(column,key){
        let newOrderBy = Object.assign({},this.state.orderBy);
        newOrderBy[key].order = newOrderBy[key].order == 'asc' ? 'desc' : 'asc';
        forEach(newOrderBy,function(value,k){
            value.active = k == key ? true : false;
        });
        let criteria = typeof(column.sortable) == 'function' ? eval(column.sortable) : column.accessor;
        let orderRows = orderBy(this.state.rows,[criteria],[newOrderBy[key]['order']]);
        this.setState({ orderBy:newOrderBy,rows:orderRows });
        if(this.props.afterOrder){
            this.props.afterOrder(orderRows);
        }
    }

    buildProps = (col, data) => {
        var props = Object.assign({},col);
        props.value = {};
        this.props.columns.map((col) => {
            if(typeof col.accessor === 'string'){
                props.value[col.accessor] = data[col.accessor];
            }
            else{
                col.accessor.map((value) => props.value[value] = data[value]);
            }
        });
        return props;
    }

    render() {
        return (
            <div className={this.props.containerClass}>
                <div className="row">
                    {this.props.excelConfiguration ? <div className="col s2">
                        <Workbook filename={this.props.excelConfiguration.titleFile} element={<button className={this.props.excelConfiguration.classButton}>{this.props.excelConfiguration.textButton}</button>}>
                            <Workbook.Sheet data={this.props.rows} name={this.props.excelConfiguration.titleSheet}>
                                {this.props.excelConfiguration.excelColumns.map((col,key) => <Workbook.Column key={key} label={col.Header} value={row => col.customSource ? col.customSource(row[col.accessor]) : row[col.accessor]}/>)}
                            </Workbook.Sheet>
                        </Workbook>
                    </div> : null}
                    <div className={this.props.excelConfiguration? 'col s10' : 'col s12'}>
                        {defaultProps.includePager
                            ? <TablePager
                                items={this.state.rows}
                                pageSize={this.props.pageSize}
                                initialPage={this.props.initialPage}
                                onChangePage={this.onChangePage}
                                renderCurrentPage={this.props.renderCurrentPage}
                                pagerContianerClass={this.props.pagerContianerClass} />
                            : null}
                    </div>
                </div>
                <div className={this.props.tableContainerClass}>
                    <table cellPadding="0" cellSpacing="0" id="table_events" className={!isEmpty(this.state.orderBy) ? 'orderable' : null}>
                        <thead>
                            <tr>
                                {this.props.columns.map((col,key) => col.visible != false ? 
                                    <th key={'head-' + key}
                                        {...col.thParams}
                                        data-orders-table={this.state.orderBy[key] && this.state.orderBy[key]['active'] ? '' : null}
                                        onClick={col.sortable ? () => this.sortBy(col,key) : null}>
                                            {col.Header}
                                            {this.state.orderBy[key] ? 
                                                this.state.orderBy[key]['order'] == 'desc' ? 
                                                <span className="order-icon material-icons">arrow_drop_down</span> : 
                                                <span className="order-icon material-icons">arrow_drop_up</span>
                                                : null}
                                    </th> 
                                : null)}
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.pageOfItems.map((row,rowKey) =>
                                (<tr key={rowKey} onDoubleClick={this.props.trDoubleClickFunction? (e) => this.props.trDoubleClickFunction(row, rowKey) : null} className={this.props.rowsClass ? this.props.rowsClass.map(rowClass => `${rowClass(row)} `) : null}>
                          {this.props.columns.map((col,colKey) =>
                                        col.visible != false ?
                                        <td key={'row-' + rowKey + '-col-' + colKey} {...col.tdParams} onClick={col.clickable? (e) => this.props.tdClickFunction(row, e) : null}>
                                                {col.Cell ? col.Cell(this.buildProps(col,row)) : row[col.accessor]}
                                        </td>
                                        : null
                                    )}
                                </tr>)
                            )}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}


AdistecTable.propTypes = propTypes;

export default AdistecTable;